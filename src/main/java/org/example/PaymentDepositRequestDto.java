package org.example;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentDepositRequestDto {
    final private String playerName;
    final private String accountNumber;
    final private String email;

    @JsonCreator
    public PaymentDepositRequestDto(@JsonProperty("playerName") String playerName,
                                    @JsonProperty("accountNumber") String accountNumber,
                                    @JsonProperty("email") String email) {
        this.playerName = playerName;
        this.accountNumber = accountNumber;
        this.email = email;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getEmail() {
        return email;
    }
}
