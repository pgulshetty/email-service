package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class EmailService {
    public static void main(String[] args) throws JsonProcessingException {

        Map<String, Object> configProps = new HashMap<>();
        // kafka server url
        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // partition key is a string
        configProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // message is also a string type
        configProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // consumer group id
        configProps.put(ConsumerConfig.GROUP_ID_CONFIG, "email-service-group");
        org.apache.kafka.clients.consumer.Consumer<String, String> consumer = new KafkaConsumer<>(configProps);

        // subscribe to topic
        consumer.subscribe(Collections.singletonList("payment-topic"));

        ObjectMapper objectMapper = new ObjectMapper();

        while (true) {

            // reading messages from kafka
            ConsumerRecords<String, String> messages = consumer.poll(Duration.ofSeconds(1));

            for (ConsumerRecord<String, String> message : messages) {
                PaymentDepositRequestDto paymentDepositRequestDto = objectMapper.readValue(message.value(), PaymentDepositRequestDto.class);
                System.out.println("Received message for player: "+paymentDepositRequestDto.getPlayerName());
                System.out.println("Sending email to address" + paymentDepositRequestDto.getEmail());
            }
        }
    }
}
